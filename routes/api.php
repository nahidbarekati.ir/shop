<?php

use App\Http\Controllers\Api\AddressController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BannerController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\BusinessSettingController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ColorController;
use App\Http\Controllers\Api\CouponController;
use App\Http\Controllers\Api\CurrencyController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\GeneralSettingController;
use App\Http\Controllers\Api\HomeCategoryController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\PasswordResetController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\PaypalController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\PurchaseHistoryController;
use App\Http\Controllers\Api\PurchaseHistoryDetailController;
use App\Http\Controllers\Api\ReviewController;
use App\Http\Controllers\Api\SettingsController;
use App\Http\Controllers\Api\ShopController;
use App\Http\Controllers\Api\SliderController;
use App\Http\Controllers\Api\StripeController;
use App\Http\Controllers\Api\SubCategoryController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\WalletController;
use App\Http\Controllers\Api\WishlistController;
use App\Http\Controllers\PolicyController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1/auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('signup', [AuthController::class, 'signup']);
    Route::post('social-login', [AuthController::class, 'socialLogin']);
    Route::post('password/create', [PasswordResetController::class, 'create']);
    Route::middleware('auth:api')->group(function () {
        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('user', [AuthController::class, 'user']);
    });
});

Route::prefix('v1')->group(function () {
    Route::apiResource('banners', BannerController::class)->only('index');

    Route::get('brands/top', [BrandController::class, 'top']);
    Route::apiResource('brands', BrandController::class)->only('index');

    Route::apiResource('business-settings', BusinessSettingController::class)->only('index');

    Route::get('categories/featured', [CategoryController::class, 'featured']);
    Route::get('categories/home', [CategoryController::class, 'home']);
    Route::apiResource('categories', CategoryController::class)->only('index');
    Route::get('sub-categories/{id}', [SubCategoryController::class, 'index'])->name('subCategories.index');

    Route::apiResource('colors', ColorController::class)->only('index');

    Route::apiResource('currencies', CurrencyController::class)->only('index');

    Route::apiResource('customers', CustomerController::class)->only('show');

    Route::apiResource('general-settings', GeneralSettingController::class)->only('index');

    Route::apiResource('home-categories', HomeCategoryController::class)->only('index');

    Route::get('purchase-history/{id}', [PurchaseHistoryController::class, 'index'])->middleware('auth:api');
    Route::get('purchase-history-details/{id}', [PurchaseHistoryDetailController::class, 'index'])->name('purchaseHistory.details')->middleware('auth:api');

    Route::get('products/admin', [ProductController::class, 'admin']);
    Route::get('products/seller', [ProductController::class, 'seller']);
    Route::get('products/category/{id}', [ProductController::class, 'category'])->name('api.products.category');
    Route::get('products/sub-category/{id}', [ProductController::class, 'subCategory'])->name('products.subCategory');
    Route::get('products/sub-sub-category/{id}', [ProductController::class, 'subSubCategory'])->name('products.subSubCategory');
    Route::get('products/brand/{id}', [ProductController::class, 'brand'])->name('api.products.brand');
    Route::get('products/todays-deal', [ProductController::class, 'todaysDeal']);
    Route::get('products/flash-deal', [ProductController::class, 'flashDeal']);
    Route::get('products/featured', [ProductController::class, 'featured']);
    Route::get('products/best-seller', [ProductController::class, 'bestSeller']);
    Route::get('products/related/{id}', [ProductController::class, 'related'])->name('products.related');
    Route::get('products/top-from-seller/{id}', [ProductController::class, 'topFromSeller'])->name('products.topFromSeller');
    Route::get('products/search', [ProductController::class, 'search']);
    Route::post('products/variant/price', [ProductController::class, 'variantPrice']);
    Route::get('products/home', [ProductController::class, 'home']);
    Route::apiResource('products', ProductController::class)->except(['store', 'update', 'destroy']);

    Route::get('carts/{id}', [CartController::class, 'index'])->middleware('auth:api');
    Route::post('carts/add', [CartController::class, 'add'])->middleware('auth:api');
    Route::post('carts/change-quantity', [CartController::class, 'changeQuantity'])->middleware('auth:api');
    Route::apiResource('carts', CartController::class)->only('destroy')->middleware('auth:api');

    Route::get('reviews/product/{id}', [ReviewController::class, 'index'])->name('api.reviews.index');

    Route::get('shop/user/{id}', [ShopController::class, 'shopOfUser'])->middleware('auth:api');
    Route::get('shops/details/{id}', [ShopController::class, 'info'])->name('shops.info');
    Route::get('shops/products/all/{id}', [ShopController::class, 'allProducts'])->name('shops.allProducts');
    Route::get('shops/products/top/{id}', [ShopController::class, 'topSellingProducts'])->name('shops.topSellingProducts');
    Route::get('shops/products/featured/{id}', [ShopController::class, 'featuredProducts'])->name('shops.featuredProducts');
    Route::get('shops/products/new/{id}', [ShopController::class, 'newProducts'])->name('shops.newProducts');
    Route::get('shops/brands/{id}', [ShopController::class, 'brands'])->name('shops.brands');
    Route::apiResource('shops', ShopController::class)->only('index');

    Route::apiResource('sliders', SliderController::class)->only('index');

    Route::get('wishlists/{id}', [WishlistController::class, 'index'])->middleware('auth:api');
    Route::post('wishlists/check-product', [WishlistController::class, 'isProductInWishlist'])->middleware('auth:api');
    Route::apiResource('wishlists', WishlistController::class)->except(['index', 'update', 'show'])->middleware('auth:api');

    Route::apiResource('settings', SettingsController::class)->only('index');

    Route::get('policies/seller', [PolicyController::class, 'sellerPolicy'])->name('policies.seller');
    Route::get('policies/support', [PolicyController::class, 'supportPolicy'])->name('policies.support');
    Route::get('policies/return', [PolicyController::class, 'returnPolicy'])->name('policies.return');

    Route::get('user/info/{id}', [UserController::class, 'info'])->middleware('auth:api');
    Route::post('user/info/update', [UserController::class, 'updateName'])->middleware('auth:api');
    Route::get('user/shipping/address/{id}', [AddressController::class, 'addresses'])->middleware('auth:api');
    Route::post('user/shipping/create', [AddressController::class, 'createShippingAddress'])->middleware('auth:api');
    Route::get('user/shipping/delete/{id}', [AddressController::class, 'deleteShippingAddress'])->middleware('auth:api');

    Route::post('coupon/apply', [CouponController::class, 'apply'])->middleware('auth:api');

    Route::post('payments/pay/stripe', [StripeController::class, 'processPayment'])->middleware('auth:api');
    Route::post('payments/pay/paypal', [PaypalController::class, 'processPayment'])->middleware('auth:api');
    Route::post('payments/pay/wallet', [WalletController::class, 'processPayment'])->middleware('auth:api');
    Route::post('payments/pay/cod', [PaymentController::class, 'cashOnDelivery'])->middleware('auth:api');

    Route::post('order/store', [OrderController::class, 'store'])->middleware('auth:api');

    Route::get('wallet/balance/{id}', [WalletController::class, 'balance'])->middleware('auth:api');
    Route::get('wallet/history/{id}', [WalletController::class, 'walletRechargeHistory'])->middleware('auth:api');
});


Route::prefix('v2/auth')->group(function () {
    Route::post('login', [App\Http\Controllers\Api\V2\AuthController::class, 'login']);
    Route::post('signup', [App\Http\Controllers\Api\V2\AuthController::class, 'signup']);
    Route::post('social-login', [App\Http\Controllers\Api\V2\AuthController::class, 'socialLogin']);
    Route::post('password/create', [App\Http\Controllers\Api\V2\PasswordResetController::class, 'create']);
    Route::middleware('auth:api')->group(function () {
        Route::get('logout', [App\Http\Controllers\Api\V2\AuthController::class, 'logout']);
        Route::get('user', [App\Http\Controllers\Api\V2\AuthController::class, 'user']);
    });
});

Route::prefix('v2')->group(function () {
    Route::apiResource('banners', App\Http\Controllers\Api\V2\BannerController::class)->only('index');

    Route::get('brands/top', [App\Http\Controllers\Api\V2\BrandController::class, 'top']);
    Route::apiResource('brands', App\Http\Controllers\Api\V2\BrandController::class)->only('index');

    Route::apiResource('business-settings', App\Http\Controllers\Api\V2\BusinessSettingController::class)->only('index');

    Route::get('categories/featured', [App\Http\Controllers\Api\V2\CategoryController::class, 'featured']);
    Route::get('categories/home', [App\Http\Controllers\Api\V2\CategoryController::class, 'home']);
    Route::get('categories/top', [App\Http\Controllers\Api\V2\CategoryController::class, 'top']);
    Route::apiResource('categories', App\Http\Controllers\Api\V2\CategoryController::class)->only('index');
    Route::get('sub-categories/{id}', [App\Http\Controllers\Api\V2\SubCategoryController::class, 'index'])->name('subCategories.index');

    Route::apiResource('colors', App\Http\Controllers\Api\V2\ColorController::class)->only('index');

    Route::apiResource('currencies', App\Http\Controllers\Api\V2\CurrencyController::class)->only('index');

    Route::apiResource('customers', App\Http\Controllers\Api\V2\CustomerController::class)->only('show');

    Route::apiResource('general-settings', App\Http\Controllers\Api\V2\GeneralSettingController::class)->only('index');

    Route::apiResource('home-categories', App\Http\Controllers\Api\V2\HomeCategoryController::class)->only('index');

    Route::get('purchase-history/{id}', [App\Http\Controllers\Api\V2\PurchaseHistoryController::class, 'index'])->middleware('auth:api');
    Route::get('purchase-history-details/{id}', [App\Http\Controllers\Api\V2\PurchaseHistoryDetailController::class, 'index'])->name('purchaseHistory.details')->middleware('auth:api');

    Route::get('products/admin', [App\Http\Controllers\Api\V2\ProductController::class, 'admin']);
    Route::get('products/seller', [App\Http\Controllers\Api\V2\ProductController::class, 'seller']);
    Route::get('products/category/{id}', [App\Http\Controllers\Api\V2\ProductController::class, 'category'])->name('api.products.category');
    Route::get('products/sub-category/{id}', [App\Http\Controllers\Api\V2\ProductController::class, 'subCategory'])->name('products.subCategory');
    Route::get('products/sub-sub-category/{id}', [App\Http\Controllers\Api\V2\ProductController::class, 'subSubCategory'])->name('products.subSubCategory');
    Route::get('products/brand/{id}', [App\Http\Controllers\Api\V2\ProductController::class, 'brand'])->name('api.products.brand');
    Route::get('products/todays-deal', [App\Http\Controllers\Api\V2\ProductController::class, 'todaysDeal']);
    Route::get('products/featured', [App\Http\Controllers\Api\V2\ProductController::class, 'featured']);
    Route::get('products/best-seller', [App\Http\Controllers\Api\V2\ProductController::class, 'bestSeller']);
    Route::get('products/related/{id}', [App\Http\Controllers\Api\V2\ProductController::class, 'related'])->name('products.related');
    Route::get('products/top-from-seller/{id}', [App\Http\Controllers\Api\V2\ProductController::class, 'topFromSeller'])->name('products.topFromSeller');
    Route::get('products/search', [App\Http\Controllers\Api\V2\ProductController::class, 'search']);
    Route::post('products/variant/price', [App\Http\Controllers\Api\V2\ProductController::class, 'variantPrice']);
    Route::get('products/home', [App\Http\Controllers\Api\V2\ProductController::class, 'home']);
    Route::apiResource('products', App\Http\Controllers\Api\V2\ProductController::class)->except(['store', 'update', 'destroy']);

    Route::get('carts/{id}', [App\Http\Controllers\Api\V2\CartController::class, 'index'])->middleware('auth:api');
    Route::post('carts/add', [App\Http\Controllers\Api\V2\CartController::class, 'add'])->middleware('auth:api');
    Route::post('carts/change-quantity', [App\Http\Controllers\Api\V2\CartController::class, 'changeQuantity'])->middleware('auth:api');
    Route::apiResource('carts', App\Http\Controllers\Api\V2\CartController::class)->only('destroy')->middleware('auth:api');

    Route::get('reviews/product/{id}', [App\Http\Controllers\Api\V2\ReviewController::class, 'index'])->name('api.reviews.index');

    Route::get('shop/user/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'shopOfUser'])->middleware('auth:api');
    Route::get('shops/details/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'info'])->name('shops.info');
    Route::get('shops/products/all/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'allProducts'])->name('shops.allProducts');
    Route::get('shops/products/top/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'topSellingProducts'])->name('shops.topSellingProducts');
    Route::get('shops/products/featured/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'featuredProducts'])->name('shops.featuredProducts');
    Route::get('shops/products/new/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'newProducts'])->name('shops.newProducts');
    Route::get('shops/brands/{id}', [App\Http\Controllers\Api\V2\ShopController::class, 'brands'])->name('shops.brands');
    Route::apiResource('shops', App\Http\Controllers\Api\V2\ShopController::class)->only('index');

    Route::apiResource('sliders', App\Http\Controllers\Api\V2\SliderController::class)->only('index');

    Route::get('wishlists/{id}', [App\Http\Controllers\Api\V2\WishlistController::class, 'index'])->middleware('auth:api');
    Route::post('wishlists/check-product', [App\Http\Controllers\Api\V2\WishlistController::class, 'isProductInWishlist'])->middleware('auth:api');
    Route::apiResource('wishlists', App\Http\Controllers\Api\V2\WishlistController::class)->except(['index', 'update', 'show'])->middleware('auth:api');

    Route::apiResource('settings', App\Http\Controllers\Api\V2\SettingsController::class)->only('index');

    Route::get('policies/seller', [App\Http\Controllers\Api\V2\PolicyController::class, 'sellerPolicy'])->name('policies.seller');
    Route::get('policies/support', [App\Http\Controllers\Api\V2\PolicyController::class, 'supportPolicy'])->name('policies.support');
    Route::get('policies/return', [App\Http\Controllers\Api\V2\PolicyController::class, 'returnPolicy'])->name('policies.return');

    Route::get('user/info/{id}', [App\Http\Controllers\Api\V2\UserController::class, 'info'])->middleware('auth:api');
    Route::post('user/info/update', [App\Http\Controllers\Api\V2\UserController::class, 'updateName'])->middleware('auth:api');
    Route::get('user/shipping/address/{id}', [App\Http\Controllers\Api\V2\AddressController::class, 'addresses'])->middleware('auth:api');
    Route::post('user/shipping/create', [App\Http\Controllers\Api\V2\AddressController::class, 'createShippingAddress'])->middleware('auth:api');
    Route::get('user/shipping/delete/{id}', [App\Http\Controllers\Api\V2\AddressController::class, 'deleteShippingAddress'])->middleware('auth:api');

    Route::post('coupon/apply', [App\Http\Controllers\Api\V2\CouponController::class, 'apply'])->middleware('auth:api');

    Route::post('payments/pay/stripe', [App\Http\Controllers\Api\V2\StripeController::class, 'processPayment'])->middleware('auth:api');
    Route::post('payments/pay/paypal', [App\Http\Controllers\Api\V2\PaypalController::class, 'processPayment'])->middleware('auth:api');
    Route::post('payments/pay/wallet', [App\Http\Controllers\Api\V2\WalletController::class, 'processPayment'])->middleware('auth:api');
    Route::post('payments/pay/cod', [App\Http\Controllers\Api\V2\PaymentController::class, 'cashOnDelivery'])->middleware('auth:api');

    Route::post('order/store', [App\Http\Controllers\Api\V2\OrderController::class, 'store'])->middleware('auth:api');

    Route::get('wallet/balance/{id}', [App\Http\Controllers\Api\V2\WalletController::class, 'balance'])->middleware('auth:api');
    Route::get('wallet/history/{id}', [App\Http\Controllers\Api\V2\WalletController::class, 'walletRechargeHistory'])->middleware('auth:api');

    Route::get('flash-deals', [App\Http\Controllers\Api\V2\FlashDealController::class, 'index']);
    Route::get('flash-deal-products/{id}', [App\Http\Controllers\Api\V2\FlashDealController::class, 'products']);
});

Route::fallback(function() {
    return response()->json([
        'data' => [],
        'success' => false,
        'status' => 404,
        'message' => 'Invalid Route'
    ]);
});
